from HW1.part1 import SkidSteer
import turtle

VISIBILITY_SCALER = 50

def _main(args=None):
    done = False
    turtle.setposition(0, 0)
    turtle.color("red")
    turtle.pendown()
    turtle.goto(5*VISIBILITY_SCALER, 0)
    turtle.goto(5*VISIBILITY_SCALER, 5*VISIBILITY_SCALER)
    turtle.goto(0, 5*VISIBILITY_SCALER)
    turtle.goto(0, 0)
    bot = SkidSteer(0.5, 0.3, color="black", vis_scale=VISIBILITY_SCALER)
    while not done:
        commands = input("Enter command: ")
        command_array = commands.split(" ")
        if command_array[0] == "quit":
            done = True
        if len(command_array) != 4:
            print("Invalid command")
        else:
            try:
                print(f"{command_array[0]}")
                converted_commmand_array = [float(i) for i in command_array]
                bot.fw_kinematics(
                    converted_commmand_array[0],
                    converted_commmand_array[1],
                    converted_commmand_array[2],
                    converted_commmand_array[3],
                )
            except ValueError:
                print("Invalid command")

if __name__ == "__main__":
    _main()
