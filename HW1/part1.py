import turtle
import typing
import time

import matplotlib.pyplot as plt
import numpy


class SkidSteer:
    """Object that is meant to simulate a skid steer robot.

    Attributes:
        length: Length of the robot.
        width: Width of the robot.
        x_0: Initial x position of the robot.
        y_0: Initial y position of the robot.
        theta_0: Initial orientation of the robot.
        sleep: Time to sleep between each position update.
        color: Color of the robot.
        vis_scale: Scalar to make the robot visible.
        x_t_minus_1: Previous x position of the robot.
        y_t_minus_1: Previous y position of the robot.
        theta_t_minus_1: Previous orientation of the robot.
    """

    def __init__(
        self,
        length: float,
        width: float,
        x_0: float = 0.0,
        y_0: float = 0.0,
        theta_0: float = 0.0,
        sleep: float = 1,
        color: str = "red",
        vis_scale: int = 10,
    ):
        """Init.

        Args:
            length: Length of the robot.
            width: Width of the robot.
            x_0: Initial x position of the robot.
            y_0: Initial y position of the robot.
            theta_0: Initial orientation of the robot.
            sleep: Time to sleep between each position update.
            color: Color of the robot.
            vis_scale: Scalar to make the robot visible.
        """
        # Bring turtle pen up and add shape
        turtle.penup()
        # set color, visibility scalar, and sleep time
        self.color = color
        self.visiblity_scale = vis_scale
        self.sleep = sleep
        # zero previous time step positions don't exist when time = 0
        self.x_t_minus_1: float = 0.0
        self.y_t_minus_1: float = 0.0
        self.theta_t_minus_1: float = 0.0
        # set initial positions
        self.x_t: float = x_0
        self.y_t: float = y_0
        self.theta_t: float = theta_0
        # create arrays to store positions for plotting
        self.x_pos: typing.List[float] = [x_0]
        self.y_pos: typing.List[float] = [y_0]
        # set dimensions of bot
        self.length = length
        self.width = width
        # set turtle position color and put pen down
        turtle.setposition(x_0, y_0)
        turtle.pencolor(color)
        turtle.pendown()
        time.sleep(sleep)

    def fw_kinematics(self, v_left: float, v_right: float, dt: float = 0.1):
        """Forward kinematics for skid steer robot.

        Args:
            v_left: Left wheel velocity.
            v_right: Right wheel velocity.
            dt: Time step of the forward kinematics. Defaults to 0.1.
        """
        # set previous time step positions
        self.x_t_minus_1 = self.x_t
        self.y_t_minus_1 = self.y_t
        self.theta_t_minus_1 = self.theta_t
        # calculate next x position with
        #   x_t-1 - 1/2*(v_left + v_right)cos(theta_t-1)dt
        self.x_t = float(
            self.x_t_minus_1
            - 0.5 * (v_right + v_left) * numpy.sin(self.theta_t_minus_1) * dt
        )
        # calculate next y position with
        #  y_t-1 + 1/2*(v_left + v_right)sin(theta_t-1)dt
        self.y_t = float(
            self.y_t_minus_1
            + 0.5 * (v_right + v_left) * numpy.cos(self.theta_t_minus_1) * dt
        )
        # calculate next theta position with
        #   theta_t-1 + 1/(width)*(v_right - v_left)dt
        self.theta_t = float(
            self.theta_t_minus_1 + (1 / self.width) * (v_right - v_left) * dt
        )
        # add positions to arrays for plotting
        self.x_pos.append(self.x_t)
        self.y_pos.append(self.y_t)
        # Added the scalar to make it visible
        turtle.goto(self.x_t * self.visiblity_scale, self.y_t * self.visiblity_scale)
        time.sleep(self.sleep)

    def break_apart(self, v_left: float, v_right: float, T: float, hertz: int):
        """Break apart large time step into 100 time steps.

        Args:
            v_left: Left wheel velocity.
            v_right: Right wheel velocity.
            dt: Time step of the forward kinematics.
        """
        time_step = 1 / hertz
        counter: float = 0
        while counter < T:
            counter += time_step
            self.fw_kinematics(v_left, v_right, time_step)

    def _show_graph(self):
        plt.xlabel("X Position")
        plt.ylabel("Y Position")
        plt.title("Robot Position")
        plt.plot(self.x_pos, self.y_pos)
        plt.savefig(f"robot_position_{self.color}.png")


def _main():
    bot_2 = SkidSteer(0.5, 0.3, sleep=0.01, color="blue", vis_scale=50)
    bot_2.break_apart(1, 1.5, 5, 10)
    bot_2.break_apart(-1, -1.5, 3, 10)
    bot_2.break_apart(0.8, -2, 8, 10)
    bot_2.break_apart(2, 2, 10, 10)
    bot_2._show_graph()
    bot = SkidSteer(0.5, 0.3)
    bot.fw_kinematics(1, 1.5, 5, .1)
    bot.fw_kinematics(-1, -1.5, 3, .1)
    bot.fw_kinematics(0.8, -2, 8, .1)
    bot.fw_kinematics(2, 2, 10, .1)
    bot._show_graph()


if __name__ == "__main__":
    _main()
